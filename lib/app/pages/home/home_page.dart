import 'package:flutter/material.dart';
import 'package:livingcity/app/controllers/home_controller.dart';
import 'package:livingcity/app/pages/login/login_page.dart';
import 'package:livingcity/app/pages/minhas_ocorrencias/minhas_ocorrencias_page.dart';
import 'package:livingcity/app/pages/nova_ocorrencia/novas_ocorrencias_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  HomeController controller;

  @override
  void initState() {
    super.initState();
    controller = HomeController();
    tabController = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Living City"),
        automaticallyImplyLeading: false,
        centerTitle: true,
        backgroundColor: Color(0XFF1A237E),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              controller.signOut().then(
                (value) {
                  return Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) {
                        return LoginPage();
                      },
                    ),
                  );
                },
              );
            },
            icon: Icon(Icons.person),
          )
        ],
        bottom: new TabBar(
          controller: tabController,
          tabs: <Widget>[
            new Tab(
              icon: new Icon(Icons.add_to_photos),
              text: "Nova ocorrência",
            ),
            new Tab(
              icon: new Icon(Icons.art_track),
              text: "Minhas ocorrências",
            ),
          ],
        ),
      ),
      body: new TabBarView(
        controller: tabController,
        children: <Widget>[
          NovasOcorrenciasPage(),
          MinhasOcorrenciasPage(),
        ],
      ),
    );
  }
}
