import 'package:flutter/material.dart';

class NovasOcorrenciasPage extends StatefulWidget {
  @override
  _NovasOcorrenciasPageState createState() => _NovasOcorrenciasPageState();
}

class _NovasOcorrenciasPageState extends State<NovasOcorrenciasPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: new Text(
          "Second Page",
          style: new TextStyle(fontSize: 25.0, color: Color(0XFF1A237E)),
        ),
      ),
    );
  }
}
