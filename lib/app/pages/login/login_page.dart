import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:livingcity/app/controllers/login_controller.dart';
import 'package:livingcity/app/pages/home/home_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginController controller;

  @override
  void initState() {
    super.initState();
    controller = LoginController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Theme.of(context).primaryColorLight,
            Theme.of(context).primaryColor,
          ],
          stops: [0.1, 0.9],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/logo.png',
                  width: 180,
                  height: 180,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Living City',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Observer(
                  builder: (_) {
                    return controller.loading
                        ? Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.yellow,
                            ),
                          )
                        : FlatButton(
                            color: Colors.green,
                            splashColor: Colors.purple,
                            onPressed: () async {
                              try {
                                controller.changeLoading(isLoading: true);
                                await controller.googleSignIn();
                              } finally {
                                controller.changeLoading(isLoading: false);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) {
                                      return HomePage();
                                    },
                                  ),
                                );
                              }
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image(
                                      image:
                                          AssetImage("assets/google_logo.png"),
                                      height: 35.0),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Login com Google',
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// return Scaffold(
//       body: Container(
//         padding: EdgeInsets.only(
//           top: 60,
//           right: 40,
//           left: 40,
//         ),
//         color: Colors.white,
//         child: ListView(
//           children: <Widget>[
//             Container(
//               height: 60,
//               alignment: Alignment.centerLeft,
//               decoration: BoxDecoration(
//                 color: Color(0XFF3C5A99),
//                 borderRadius: BorderRadius.all(
//                   Radius.circular(5),
//                 ),
//               ),
//               child: SizedBox.expand(
//                 child: Observer(builder: (_) {
//                   return controller.loading
//                       ? Center(
//                           child: CircularProgressIndicator(),
//                         )
//                       : RaisedButton(
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: <Widget>[
//                               Text(
//                                 "Login com Google",
//                                 style: TextStyle(
//                                   fontWeight: FontWeight.bold,
//                                   color: Colors.white,
//                                   fontSize: 20,
//                                 ),
//                                 textAlign: TextAlign.left,
//                               ),
//                               Container(
//                                 child: SizedBox(
//                                   child: Image.asset("assets/fb-icon.png"),
//                                   height: 28,
//                                   width: 28,
//                                 ),
//                               ),
//                             ],
//                           ),
//                           onPressed: () async {
//                             await controller.googleSignIn().then((value) {

//                               );
//                             });
//                           },
//                         );
//                 }),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
