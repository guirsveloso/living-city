import 'package:livingcity/app/services/auth/auth_service.dart';
import 'package:mobx/mobx.dart';
part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  AuthService authService = AuthService();

  @observable
  bool loading = false;

  @action
  void changeLoading({bool isLoading}) {
    isLoading = isLoading;
  }

  @action
  Future googleSignIn() async {
    try {
      loading = true;
      await authService.googleSignIn();
    } catch (e) {
      loading = false;
      print('Google Sign In error: $e');
      return null;
    }
    loading = false;
  }
}
