import 'package:firebase_auth/firebase_auth.dart';
import 'package:livingcity/app/services/auth/auth_service.dart';
import 'package:mobx/mobx.dart';
part 'auth_controller.g.dart';

class AuthController = _AuthControllerBase with _$AuthController;

abstract class _AuthControllerBase with Store {
  AuthService authService = AuthService();

  _AuthControllerBase(this.authService);

  @observable
  FirebaseUser user;

  @action
  Future setUser() async {
    try {
      await authService.setUser(user);
      print(user.uid);
    } catch (e) {
      print('ERROR: $e');
    }
  }
}
