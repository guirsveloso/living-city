import 'package:firebase_auth/firebase_auth.dart';

abstract class IAuthRepository {
  Future<FirebaseUser> googleSignIn();
  Future<FirebaseUser> getUser();
  Future signOut();
}
