import 'package:flutter/material.dart';
import 'package:livingcity/app/pages/login/login_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Living City',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Color(0xff7159c1),
          primaryColorLight: Color(0xff9B49c1)),
      home: LoginPage(),
    );
  }
}
